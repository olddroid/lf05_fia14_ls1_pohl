import java.util.Scanner;

public class Nutzerdaten {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Salam aleikum shabaab, wie alt bist du? ");
		int alter = myScanner.nextInt();
		System.out.print("Und dein Name? ");
		String name = myScanner.next();
		System.out.print("Danke dir " + name + ". Du bist " + alter + " Jahre alt.");
		myScanner.close();
	}

}
