public class Konfiguration {
	public static void main(String[] args) {
		String typ = "Automat AVR";
		String bezeichnung = "Q2021_FAB_A";
		String name = typ + " " + bezeichnung;
		
		char sprachModul = 'd';
		final byte PRUEFNR = 4;
		
		int	euro;
		int cent;
		int summe;
		int muenzenEuro = 130;
		int muenzenCent = 1280;
		
		double maximum = 100.00;
		double patrone = 46.24;
		double prozent;
		
		boolean status;
		
		summe = muenzenCent + muenzenEuro * 100;
		euro = summe / 100;
		cent = summe % 100;
		
		prozent = maximum - patrone;
		
		status = (euro <= 150) && (prozent >= 50.00) && (euro >= 50) && (cent != 0) && (sprachModul == 'd') &&  (!(PRUEFNR == 5 || PRUEFNR == 6)); 
		
		System.out.println("Name: " + name);
		System.out.println("Sprache: " + sprachModul);
		System.out.println("Pr�fnummer : " + PRUEFNR);
		System.out.println("F�llstand Patrone: " + prozent + " %");
		System.out.println("Summe Euro: " + euro +  " Euro");
		System.out.println("Summe Rest: " + cent +  " Cent");		
		System.out.println("Status: " + status);
	}
}