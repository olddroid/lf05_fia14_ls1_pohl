import java.util.Scanner; // Import der Klasse Scanner 

public class Rechner {
	public static void main(String[] args) {// Hier startet das Programm
		
		// Neues Scanner-Objekt myScanner wird erstellt
		Scanner myScanner = new Scanner(System.in);

		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

		// Die Variable zahl1 speichert die erste Eingabe
		int zahl1 = myScanner.nextInt();

		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

		// Die Variable zahl2 speichert die zweite Eingabe
		int zahl2 = myScanner.nextInt();
		
		System.out.print("Rechenoperation? 1 Addition, 2 Subtraktion, 3 Multiplikation, 4 Division: ");
		int rechenoperation = myScanner.nextInt();

		// Die Addition der Variablen zahl1 und zahl2
		// wird der Variable ergebnis zugewiesen.
		int ergebnis = 0;
		if(rechenoperation == 1) {
			ergebnis = zahl1 + zahl2;
			System.out.print("\n\nErgebnis der Addition lautet: ");
			System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);
		} else if(rechenoperation == 2) {
			ergebnis = zahl1 - zahl2;
			System.out.print("\n\nErgebnis der Subtraktion lautet: ");
			System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnis);
		} else if(rechenoperation == 3) {
			ergebnis = zahl1 * zahl2;
			System.out.print("\n\nErgebnis der Multiplikation lautet: ");
			System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnis);
		} else if(rechenoperation == 4) {
			ergebnis = zahl1 / zahl2;
			System.out.print("\n\nErgebnis der Division lautet: ");
			System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnis);
		} else {
			ergebnis = -0;
			System.out.print("\n\nFehler!");
		}

		myScanner.close();
	}
}