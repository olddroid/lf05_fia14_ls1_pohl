import java.util.Scanner;

public class Konsolenausgabe {
	
	public static void main(String[] args) {
		
		//A1.5 AB Ausgabeformatierung 1 Aufgabe 1
		/*String satz1 = "Soll endlich ";
		String satz2 = "ein \"Ende\" haben.";
		
		System.out.print("Corona ist doof. ");
		//println() setzt einen Zeilenumbruch nach dem ausgegebenen Text, print() tut dies nicht.
		System.out.println(satz1 + satz2);
		System.out.println("");
		
		
		//A1.5 AB Ausgabeformatierung 1 Aufgabe 2
		String stern3 = "***";
		
		System.out.printf("%7s\n", "*");
		System.out.printf("%8s\n", stern3);
		System.out.printf("%9s\n", "*****");
		System.out.printf("%10s\n", "*******");
		System.out.printf("%11s\n", "*********");
		System.out.printf("%12s\n", "***********");
		System.out.printf("%13s\n", "*************");
		System.out.printf("%8s\n", stern3);
		System.out.printf("%8s\n", stern3);
		System.out.println("");

		
		//A1.5 AB Ausgabeformatierung 1 Aufgabe 3
		System.out.printf("%.2f\n", 22.4234234);
		System.out.printf("%.2f\n", 111.2222);
		System.out.printf("%.2f\n", 4.0);
		System.out.printf("%.2f\n", 1000000.551);
		System.out.printf("%.2f\n", 97.34);
		System.out.println("");
		
		
		//A1.6 AB Ausgabeformatierung 2 Aufgabe 1 (Ohne Leerzeichen)
		System.out.printf("%5s\n", "**");
		System.out.print("*");
		System.out.printf("%7s\n", "*");
		System.out.print("*");
		System.out.printf("%7s\n", "*");
		System.out.printf("%5s\n", "**");
		System.out.println("");
		
		
		//A1.6 AB Ausgabeformatierung 2 Aufgabe 2
		System.out.printf("%-5s = ", "!0");
		System.out.printf("%-19s = ", "");
		System.out.printf("%4s\n", "1");
		
		System.out.printf("%-5s = ", "!1");
		System.out.printf("%-19s = ", "1");
		System.out.printf("%4s\n", "1");
		
		System.out.printf("%-5s = ", "!2");
		System.out.printf("%-19s = ", "1 * 2");
		System.out.printf("%4s\n", "2");
		
		System.out.printf("%-5s = ", "!3");
		System.out.printf("%-19s = ", "1 * 2 * 3");
		System.out.printf("%4s\n", "6");
		
		System.out.printf("%-5s = ", "!4");
		System.out.printf("%-19s = ", "1 * 2 * 3 * 4");
		System.out.printf("%4s\n", "24");
		
		System.out.printf("%-5s = ", "!5");
		System.out.printf("%-19s = ", "1 * 2 * 3 * 4 * 5");
		System.out.printf("%4s\n", "120");
		System.out.println("");
		
		
		//A1.6 AB Ausgabeformatierung 2 Aufgabe 3
		System.out.printf("%-12s|", "Fahrenheit");
		System.out.printf("%10s\n", "Celsius");
		System.out.println("-----------------------");
		
		System.out.printf("%-12s|", "-20");
		System.out.printf("%10.2f\n", -28.8889);
		
		System.out.printf("%-12s|", "-10");
		System.out.printf("%10.2f\n", -23.3333);
		
		System.out.printf("%-12s|", "+0");
		System.out.printf("%10.2f\n", -17.7778);
		
		System.out.printf("%-12s|", "+20");
		System.out.printf("%10.2f\n", -6.6667);
		
		System.out.printf("%-12s|", "+30");
		System.out.printf("%10.2f\n", -1.1111);*/
		
		Scanner scanner = new Scanner(System.in);
		int zahl1 = scanner.nextInt();
		int zahl2 = scanner.nextInt();
		int zahl3 = scanner.nextInt();
		if(zahl1 > zahl2 && zahl1 > zahl3) {
			System.out.print("Zahl 1 ist am gr��ten");
		} else if (zahl2 > zahl1 && zahl2 > zahl3) {
			System.out.print("Zahl 2 ist am gr��ten");
		} else if (zahl3 > zahl1 && zahl3 > zahl2) {
			System.out.print("Zahl 3 ist am gr��ten");
		} else {
			System.out.print("Zahlen sind gleich gro�");
		}
	}

}