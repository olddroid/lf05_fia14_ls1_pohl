package utils;

public class Bestellung {
	private byte anzahlFahrscheine;
	private double ticketPreis;
	private double gesamtBetrag;
	
	public Bestellung(byte anzahlFahrscheine, double ticketPreis) {
		this.anzahlFahrscheine = anzahlFahrscheine;
		this.ticketPreis = ticketPreis;
		gesamtBetrag = anzahlFahrscheine * ticketPreis;
	}
	
	public double getTicketPreis() {
		return ticketPreis;
	}
	
	public byte getAnzahlFahrscheine() {
		return anzahlFahrscheine;
	}
	
	public double getGesamtBetrag() {
		return gesamtBetrag;
	}
}